import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoginformationsPage } from './doginformations.page';

describe('DoginformationsPage', () => {
  let component: DoginformationsPage;
  let fixture: ComponentFixture<DoginformationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoginformationsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoginformationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
