import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { HttpClient } from '@angular/common/http';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
 
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NavExtrasService } from '../services/nav-extras.service';
 
const STORAGE_KEY = 'my_images';
@Component({
  selector: 'app-doginformations',
  templateUrl: './doginformations.page.html',
  styleUrls: ['./doginformations.page.scss'],
})
export class DoginformationsPage implements OnInit {
  name:string;
  pere:string;
  mere:string;
  sexe:string;
  phone:string;
  datenaiss: Date;
  adresse:string;
  imgData;
  proprietaire:string;
  constructor(private navExtras: NavExtrasService, private file: File, private http: HttpClient, private webview: WebView,
    private actionSheetController: ActionSheetController, private toastController: ToastController,
    private storage: Storage, private plt: Platform, private loadingController: LoadingController,
    private ref: ChangeDetectorRef, private filePath: FilePath, private router: Router) { 
    this.imgData =  this.navExtras.getExtras();
  }
  
  ngOnInit() {
    
    
  }
  updateMyDate($event){
    console.log($event)
    this.datenaiss = $event
  }
  upload(){
    //console.log("datas of dog:",this.datenaiss);
     this.file.resolveLocalFilesystemUrl(this.imgData.filePath)
      .then(entry => {
          ( < FileEntry > entry).file(file => this.readFile(file))
      })
      .catch(err => {
          this.presentToast('Error while reading file.');
      }); 
  }
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }
  readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
        const formData = new FormData();
        const imgBlob = new Blob([reader.result], {
            type: file.type
        });
        formData.append('file', imgBlob, file.name);
        formData.append('name',this.name)
        formData.append('pere',this.pere)
        formData.append('mere',this.mere)
        formData.append('proprietaire',this.proprietaire)
        formData.append('sexe',this.sexe)
        formData.append('phone',this.phone)
        formData.append('adresse',this.adresse)
        formData.append('datenaiss',this.datenaiss+"")
        this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }
  
  async uploadImageData(formData: FormData) {
    const loading = await this.loadingController.create({
      message: 'Uploading datas...',
    });
    await loading.present();
    console.log("data to send",formData)
    this.http.post("http://tiokolane.pythonanywhere.com/api/dog/", formData)
        .pipe(
            finalize(() => {
                loading.dismiss();
                this.router.navigateByUrl('/home');
            })
        )
        .subscribe(res => {
          console.log("result",res)
            if (res['status']=="success") {
                this.presentToast('File upload complete.')
                console.log("success")
            } else {
                this.presentToast('File upload failed.')
                console.log("error",res)
            }
        });
  }
}
