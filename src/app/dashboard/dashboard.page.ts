import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
 
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NavExtrasService } from '../services/nav-extras.service';
import { Observable } from 'rxjs/Observable';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  datas;
  dogs;

  constructor(private navExtras: NavExtrasService, private file: File, private http: HttpClient, private webview: WebView,
    private actionSheetController: ActionSheetController, private toastController: ToastController,
    private storage: Storage, private plt: Platform, private loadingController: LoadingController,
    private ref: ChangeDetectorRef, private filePath: FilePath, private router: Router) { 
      this.loadDogs();
    }

  ngOnInit() {
    
  }
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }
  async loadDogs() {
    const loading = await this.loadingController.create({
      message: 'Loading datas...',
    });
    await loading.present();
    let headers: HttpHeaders = new HttpHeaders();
    headers.append("cache-control", "no-cache");
    this.http.get('http://tiokolane.pythonanywhere.com/api/dog/',{"headers": headers})
    .subscribe((response) => {
      console.log(response);
      loading.dismiss();
      this.datas = response
      this.dogs = this.datas.data
      console.log(this.dogs)
  })
  
}
}
